/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author sarit
 */
public class Lab02 {

    static char[][]GameBoard = {{' ', '|',' ','|',' '},
                {'-', '+', '-','+', '-'},
                {' ', '|', ' ','|', ' '},
                {'-', '+', '-','+', '-'},
                {' ', '|', ' ','|', ' '}};
    static char currentPlayer = 'O';
    static int Pos;
    
    public static void main(String[] args) {
        printWelcome();
        printGameBoard();
        
        while (!isGameOver()){
            printTurn();
            inputPos();
            printGameBoard();
            switchPlayer();
        }
        
    }

    private static void printWelcome() {
        System.out.println("welcome to XO Game");
    }

    private static void printGameBoard() {
        for(int i=0;i<GameBoard.length;i++){
            for(int j=0;j<GameBoard.length;j++){
                System.out.print(GameBoard[i][j]);
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("Turn "+ currentPlayer);
    }

    private static void inputPos() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input Position: ");
        Pos = kb.nextInt();
        updateGameBoard();
        
    }
    
    private static void updateGameBoard() {
        int row = Pos /3;
        int col = Pos % 3;
        
        if(GameBoard[row * 2 + 1][col * 2] == ' '){
            GameBoard[row*2 +1][col *2]= currentPlayer;
        }else{
            System.out.println("Invalid position. Please choose an empty position.");
            inputPos();
        }
        
    }

    private static void switchPlayer() {
        currentPlayer = (currentPlayer == 'O') ? 'X': 'O';
    }

    private static boolean isGameOver() {
        for(int i = 0; i<GameBoard.length; i+= 2){
            if (GameBoard[i][0] != ' ' && GameBoard[i][0] == GameBoard[i][2] && GameBoard[i][0] == GameBoard[i][4]) {
                return true;
            }
        }
        
        for(int i=0; i< GameBoard.length; i+= 2){
            if (GameBoard[0][i] != ' ' && GameBoard[0][i] == GameBoard[2][i] && GameBoard[0][i] == GameBoard[4][i]) {
                return true;
            }
        }
        
        if(GameBoard[0][0] != ' '&& GameBoard[0][0] == GameBoard[2][2]&& GameBoard[0][0] == GameBoard[4][4]) {
            return true;
        }
        if(GameBoard[0][4] != ' '&& GameBoard[0][4] == GameBoard[2][2] && GameBoard[0][4] == GameBoard[4][0]){
            return true;
        }
        
        for (int i = 0;i<GameBoard.length; i += 2){
            for(int j = 0;j< GameBoard.length; j += 2){
                if(GameBoard[i][j] == ' '){
                    return false;
                }
            }
        }
        return true;
    }
    
}
